vip-manager2 (3.0.0-2) unstable; urgency=medium

  [ Christoph Berg ]
  * Refresh debian/patches/etcd-3.4-build-fixes.patch.

  [ Michael Banck ]
  * debian/patches/etcd-3.4-build-fixes.patch: Updated with reverts to Logger
    changes. 

 -- Christoph Berg <myon@debian.org>  Mon, 03 Feb 2025 16:33:05 +0100

vip-manager2 (3.0.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/disable-consul: Refreshed.
  * debian/patches/go_deps.patch: Likewise.
  * debian/rules (override_dh_auto_build): New rule, uses version and date from
    the Debian changelog to set the build flags for --version output. (Closes:
    Salsa#1)

 -- Michael Banck <mbanck@debian.org>  Mon, 13 Jan 2025 16:19:22 +0100

vip-manager2 (2.8.0-1) unstable; urgency=medium

  * New upstream version 2.8.0.

 -- Christoph Berg <myon@debian.org>  Mon, 04 Nov 2024 17:03:28 +0100

vip-manager2 (2.7.0-1) unstable; urgency=medium

  * New upstream version 2.7.0.

 -- Christoph Berg <myon@debian.org>  Wed, 25 Sep 2024 16:12:03 +0200

vip-manager2 (2.6.0-1) unstable; urgency=medium

  * New upstream version 2.6.0.

 -- Christoph Berg <myon@debian.org>  Tue, 03 Sep 2024 17:59:17 +0200

vip-manager2 (2.5.0-2) unstable; urgency=medium

  * Update import paths for etcd v3.5 (Closes: #1078323)

 -- Bradford D. Boyle <bradford.d.boyle@gmail.com>  Fri, 23 Aug 2024 11:57:04 +0200

vip-manager2 (2.5.0-1) unstable; urgency=medium

  * New upstream version 2.5.0.
  * debian/tests: Use dashes in test names.

 -- Christoph Berg <myon@debian.org>  Fri, 17 May 2024 10:12:20 +0200

vip-manager2 (2.3.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/behaviour_test.patch: Refreshed.
  * debian/patches/go_deps.patch: Likewise.
  * debian/patches/etcd-3.4-build-fixes.patch: Likewise.

 -- Michael Banck <mbanck@debian.org>  Sat, 10 Feb 2024 16:15:05 +0100

vip-manager2 (2.2.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/disable-consul: Refrehed.
  * debian/patches/go_deps.patch: Likewise.
  * debian/patches/support_etcd3_as_endpoint_type.patch: Removed, applied
    upstream.

 -- Michael Banck <mbanck@debian.org>  Fri, 19 Jan 2024 17:15:23 +0100

vip-manager2 (2.1.0-5) unstable; urgency=medium

  * Install systemd services using dh_installsystemd only. (See #1054479)
  * debian/tests: Set ETCD_UNSUPPORTED_ARCH on s390x.
  * Add myself to Uploaders.

 -- Christoph Berg <myon@debian.org>  Tue, 24 Oct 2023 14:43:38 +0000

vip-manager2 (2.1.0-4) unstable; urgency=medium

  * debian/tests/test: Make sure etcd is stopped before tests start (Closes:
    #1030967). 

 -- Michael Banck <mbanck@debian.org>  Fri, 17 Feb 2023 14:40:18 +0100

vip-manager2 (2.1.0-3) unstable; urgency=medium

  * debian/control (Uploaders): Updated.

 -- Michael Banck <mbanck@debian.org>  Fri, 17 Feb 2023 13:12:42 +0100

vip-manager2 (2.1.0-2) unstable; urgency=medium

  * debian/vip-manager.docs: Renamed to ...
  * debian/vip-manager2.docs: ... this.

 -- Michael Banck <michael.banck@credativ.de>  Wed, 08 Feb 2023 11:31:12 +0100

vip-manager2 (2.1.0-1) unstable; urgency=medium

  * Initial upload of vip-manager2, based off on vip-manager packaging.

 -- Michael Banck <michael.banck@credativ.de>  Tue, 07 Feb 2023 17:45:47 +0100
